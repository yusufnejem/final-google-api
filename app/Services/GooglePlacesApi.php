<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use App\Exceptions\GooglePlacesApiException;

class GooglePlacesApi
{
    const BASE_URL = 'https://maps.googleapis.com/maps/api/place/';
    
    const TEXT_SEARCH_URL = 'textsearch/json';
    
    const DETAILS_SEARCH_URL = 'details/json';
    
    /**
     * @var
     */
    public $status;
    
    /**
     * @var null
     */
    private $key = null;
    
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;
    
    /**
     * @var bool
     */
    private $verifySSL = true;
    
    /**
     * GooglePlacesApi constructor.
     *
     * @param null $key
     * @param bool $verifySSL
     */
    public function __construct($key = null, $verifySSL = true)
    {
        $this->key = $key;
        
        $this->client = new Client([
            'base_uri' => self::BASE_URL,
        ]);
    }
    
    /**
     * Place Text Search Request to google places api.
     *
     * @param $query
     * @param array $params
     *
     * @return \Illuminate\Support\Collection
     * @throws \App\Exceptions\GooglePlacesApiException
     */
    public function textSearch($query, $params = [])
    {
        $this->checkKey();
        
        $params['query'] = $query;
        $response = $this->makeRequest(self::TEXT_SEARCH_URL, $params);
        
        return $this->convertToCollection($response, 'results');
        
    }
 
    /**
     * Place Details Request to google places api.
     *
     * @param $placeId
     * @param array $params
     *
     * @return \Illuminate\Support\Collection
     * @throws \App\Exceptions\GooglePlacesApiException
     */
    public function placeDetails($placeId, $params = [])
    {
        $this->checkKey();
        
        $params['placeid'] = $placeId;
        
        $response = $this->makeRequest(self::DETAILS_SEARCH_URL, $params);
        
        return $this->convertToCollection($response);
    }
    
    /**
     * @param $uri
     * @param $params
     * @param $method
     *
     * @return mixed|string
     * @throws \App\Exceptions\GooglePlacesApiException
     */
    private function makeRequest($uri, $params, $method = 'get')
    {
        $options = $this->getOptions($params, $method);
        
        $response = json_decode(
            $this->client->$method($uri, $options)->getBody()->getContents(),
            true
        );
        
        $this->setStatus($response['status']);
        
        if ($response['status'] !== 'OK'
            AND $response['status'] !== 'ZERO_RESULTS') {
            throw new GooglePlacesApiException(
                "Response returned with status: " . $response['status'] . "\n" .
                array_key_exists('error_message', $response)
                    ?: "Error Message: {$response['error_message']}"
            );
        }
        
        return $response;
    }
    
    /**
     * @param array $data
     * @param null $index
     *
     * @return \Illuminate\Support\Collection
     */
    private function convertToCollection(array $data, $index = null)
    {
        $data = collect($data);
        
        if ($index) {
            $data[$index] = collect($data[$index]);
        }
        
        return $data;
    }
    
    /**
     * @param mixed $status
     */
    private function setStatus($status)
    {
        $this->status = $status;
    }
    
    /**
     * @throws \App\Exceptions\GooglePlacesApiException
     */
    private function checkKey()
    {
        if (!$this->key) {
            throw new GooglePlacesApiException('API KEY is not specified.');
        }
    }
    
    /**
     * @param bool $verifySSL
     *
     * @return GooglePlacesApi
     */
    public function verifySSL($verifySSL = true)
    {
        $this->verifySSL = $verifySSL;
        
        return $this;
    }
    
    /**
     * @param array $params
     * @param string $method
     *
     * @return array
     */
    private function getOptions($params, $method = 'get')
    {
        $options = [
            'query' => [
                'key' => $this->key,
            ],
        ];
        
        if ($method == 'post') {
            $options = array_merge(['body' => json_encode($params)], $options);
        } else {
            $options['query'] = array_merge($options['query'], $params);
        }
        
        $options['http_errors'] = false;
        
        $options['verify'] = $this->verifySSL;
        
        return $options;
    }
}
