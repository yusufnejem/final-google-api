<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\GooglePlacesApi;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GooglePlaces', function ($app) {
            $key = isset($app['config']['google.places.key'])
                ? $app['config']['google.places.key'] : null;

            return new GooglePlacesApi($key);
        });
    }
}
